# automated_machine_learning

* Code to perform automated machine learning using the TPOT library. This uses data from the UCI machine learning repository.

* Usage:

    * python3 automated_machine_learning_uci.py
    
* Adapted from:

    * https://github.com/EpistasisLab/tpot
    
    

