#####################################################
# Automated machine learning
#       applied to a dataset from the UCI machine
#       learning repository
#
# INSTALLATION:
#   pip3 install -U scikit-learn
#   pip3 install deap update_checker tqdm stopit
#   pip3 install tpot
#   pip3 install xgboost
#   pip3 install tensorflow
#       https://www.tensorflow.org/install/install_mac
#   sudo pip3 install keras
#       https://keras.io/#installation

#
#
# Usage:
#   python3 automated_machine_learning_uci.py
#
# Adapted from:
#   https://github.com/EpistasisLab/tpot
#   https://github.com/rasbt/python-machine-learning-book/blob/master/code/ch06/ch06.ipynb
#
#####################################################


###################################################
# Load libraries
###################################################
import keras
from keras.utils import to_categorical
from keras.utils import normalize

import sklearn
# import scikit
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import cross_val_score
from sklearn.metrics import roc_auc_score, accuracy_score
from sklearn.metrics import roc_curve, auc
from scipy import interp

import numpy as np
import pandas as pd
import pdb

import matplotlib.pyplot as plt
import seaborn as sns

from sklearn.ensemble import RandomForestClassifier
from tpot import TPOTClassifier
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split

#####################################################################################
#  Load data
#####################################################################################
print("\n ********** Data Loading Section ********** \n")
print("Loading dataset: \n")

# read in pandas dataframe
# generated from breast-cancer-wisconsin_MOD.data using data_munging.R
temp_str_peptide_file = "breast-cancer-wisconsin_MOD_CURATED.data"
temp_peptide_df = pd.read_csv(temp_str_peptide_file, header=None)
#            temp_peptide_df.columns = ['peptide', 'abundance']

#####################################################################################
# Split data into training and test set
#####################################################################################
print("\n ********** Train Test Split Section ********** \n")
print("Splitting data into training and test set: \n")

i_split_train_test_from = 500
x_train = temp_peptide_df.iloc[0:i_split_train_test_from, 1:-1]  # temp_peptide_df["epithelial_cell_size"]
y_train = temp_peptide_df.iloc[0:i_split_train_test_from, -1]  # temp_peptide_df["class"]

x_train_array = np.array(x_train)
# y_train_array = np.array(y_train)


#####################################################################################
# Data munging
#####################################################################################


#####################################################################################
# Feature scaling
#####################################################################################
print("\n ********** Data Munging Section ********** \n")
print("Performing feature scaling: \n")

x_train_array = keras.utils.normalize(x_train_array)
# y_train_array = keras.utils.normalize(y_train_array)

# y_binary = to_categorical(y_int)
# y_train = to_categorical(y_train)#,2)


#####################################################################################
# Evaluate your performance in one line:
#####################################################################################
x_test = temp_peptide_df.iloc[i_split_train_test_from:, 1:-1]  # temp_peptide_df["epithelial_cell_size"]
y_test = temp_peptide_df.iloc[i_split_train_test_from:, -1]  # temp_peptide_df["class"]

x_test_array = np.array(x_test)
# feature scaling
x_test_array = keras.utils.normalize(x_test_array)

# y_test = to_categorical(y_test)#,2)

#####################################################################################
# Visualize balance or imbalance of training data
#####################################################################################
plt.figure(figsize=(8, 4))
sns.countplot(x=y_train)
plt.savefig('balance_trainingset.png', dpi=300)

plt.figure(figsize=(8, 4))
sns.countplot(x=y_test)
plt.savefig('balance_testset.png', dpi=300)

##################################################################
# Baseline model
##################################################################

# Random forest
rf = RandomForestClassifier(max_depth=3, n_estimators=10)
rf.fit(x_train, y_train)

y_pred_rf = rf.predict_proba(x_test)[:, 1]
fpr_rf, tpr_rf, thresholds_rf = roc_curve(y_test, y_pred_rf)
auc_rf = auc(fpr_rf, tpr_rf)
print("AUC from a baseline random forest algorithm: ", auc_rf)

##################################################################
# Automated machine learning using TPOT
##################################################################

# reformat for TPOT (numpy arrays)
#x_train_array = np.array(x_train)
y_train_array = np.array(y_train)

X_train, X_test, Y_train, Y_test = train_test_split(x_train_array,
                                                    y_train_array, train_size=0.75, test_size=0.25)

tpot = TPOTClassifier(generations=10, population_size=50, verbosity=2)
tpot.fit(X_train, Y_train)
print("Score from a baseline automated machine learning algorithm: ", tpot.score(X_test, Y_test))

##################################################################
# Save best model as python program
##################################################################
tpot.export('tpot_uci.py')


print("\n ***************************************** \n")
print("   All tasks successfully completed \n")
print(" ***************************************** \n")
